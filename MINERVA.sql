CREATE TABLE IF NOT EXISTS cliente (
    Identificacion VARCHAR(200),
    TipoIdentificacion VARCHAR(200) NOT NULL,
    Nombres VARCHAR(200) NOT NULL,
    Apellidos VARCHAR(200) NOT NULL,
    Celular VARCHAR(200) UNIQUE NOT NULL,
    Correo VARCHAR(200) UNIQUE NOT NULL,
    Direccion VARCHAR(200) NOT NULL,
    Monto_de_retencion VARCHAR(200) NOT NULL,
    PRIMARY KEY(Identificacion)
);

CREATE TABLE IF NOT EXISTS empleado (
    Identificacion VARCHAR(200),
    TipoIdentificacion VARCHAR(200) NOT NULL,
    Nombres VARCHAR(200) NOT NULL,
    Apellidos VARCHAR(200) NOT NULL,
    Celular VARCHAR(200) UNIQUE NOT NULL,
    Correo VARCHAR(200) UNIQUE NOT NULL,
    Direccion VARCHAR(200) NOT NULL,
    Eps VARCHAR(200) NOT NULL,
    Arl VARCHAR(200) NOT NULL,
    Pension VARCHAR(200) NOT NULL,
    PRIMARY KEY(Identificacion)
);


CREATE TABLE IF NOT EXISTS factura (
	id INT PRIMARY KEY AUTO_INCREMENT,
    empleado_id VARCHAR(200) NOT NULL,
    cliente_id VARCHAR(200) NOT NULL,
    fecha VARCHAR(200) NOT NULL,
    forma_de_pago VARCHAR(200) NOT NULL,
    rete_fuente FLOAT NOT NULL,
    rete_iva FLOAT NOT NULL,
    rete_ica FLOAT NOT NULL,
    subtotal FLOAT NOT NULL,
    iva FLOAT NOT NULL,
    descuento FLOAT NOT NULL,
    total FLOAT NOT NULL,
    estado INT NOT NULL
);

ALTER TABLE factura ADD FOREIGN KEY fk_emp_fac(empleado_id) REFERENCES empleado(Identificacion) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE factura ADD FOREIGN KEY fk_cli_fac(cliente_id) REFERENCES cliente(Identificacion) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS credito (
	id INT PRIMARY KEY AUTO_INCREMENT,
    cliente_id VARCHAR(200) NOT NULL,
    factura_id INT,
    dias_de_pago INT NOT NULL,
    fecha VARCHAR(200) NOT NULL,
    hora VARCHAR(200) NOT NULL,
    monto_abonado FLOAT NOT NULL,
    estado VARCHAR(200) NOT NULL
);

ALTER TABLE credito ADD FOREIGN KEY fk_cli_cre(cliente_id) REFERENCES cliente(Identificacion) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE credito ADD FOREIGN KEY fk_fac_cre(factura_id) REFERENCES factura(id) ON UPDATE CASCADE ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS producto (
	codigo INT PRIMARY KEY,
    nombre VARCHAR(200) NOT NULL,
    marca VARCHAR(200) NOT NULL,
    categoria VARCHAR(200) NOT NULL,
    stock INT NOT NULL,
    iva FLOAT NOT NULL,
    precio FLOAT NOT NULL,
    descripcion TEXT,
    ref1 VARCHAR(200) NOT NULL,
    ref2 VARCHAR(200) NOT NULL,
    ref3 VARCHAR(200) NOT NULL,
    ref4 VARCHAR(200) NOT NULL
);