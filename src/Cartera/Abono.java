/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cartera;

import Ventas.Forma_de_pago;
import java.sql.Time;
import java.util.*;

/**
 *
 */
public class Abono {

    /**
     * Default constructor
     */
    public Abono() {
        
    }

    /**
     *
     */
    private int ID;

    /**
     *
     */
    private Date Fecha;

    /**
     *
     */
    private Time Hora;

    /**
     *
     */
    private Credito Credito;

    /**
     *
     */
    private Float Valor;

    /**
     *
     */
    private String Forma_de_pago;

    public Abono(int ID, Date Fecha, Time Hora, Credito Credito, Float Valor, String Forma_de_pago) {
        this.ID = ID;
        this.Fecha = Fecha;
        this.Hora = Hora;
        this.Credito = Credito;
        this.Valor = Valor;
        this.Forma_de_pago = Forma_de_pago;
    }
    

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public Time getHora() {
        return Hora;
    }

    public void setHora(Time Hora) {
        this.Hora = Hora;
    }

    public Credito getCredito() {
        return Credito;
    }

    public void setCredito(Credito Credito) {
        this.Credito = Credito;
    }

    public Float getValor() {
        return Valor;
    }

    public void setValor(Float Valor) {
        this.Valor = Valor;
    }

    public String getForma_de_pago() {
        return Forma_de_pago;
    }

    public void setForma_de_pago(String Forma_de_pago) {
        this.Forma_de_pago = Forma_de_pago;
    }
    
    

}
