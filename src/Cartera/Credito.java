/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cartera;
import Ventas.Factura;
import java.sql.Time;
import java.util.*;

/**
 * 
 */
public class Credito {

    private int ID;
    private String id_cliente;
    private int Dias_de_pago;
    private String Fecha;
    private String Hora;
    private Float Monto_Abonado;
    private int id_factura;
    private String Estado;

    public Credito(int ID, String id_cliente, int Dias_de_pago, String Fecha, String Hora, Float Monto_Abonado, int id_factura, String Estado) {
        this.ID = ID;
        this.id_cliente = id_cliente;
        this.Dias_de_pago = Dias_de_pago;
        this.Fecha = Fecha;
        this.Hora = Hora;
        this.Monto_Abonado = Monto_Abonado;
        this.id_factura = id_factura;
        this.Estado = Estado;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getDias_de_pago() {
        return Dias_de_pago;
    }

    public void setDias_de_pago(int Dias_de_pago) {
        this.Dias_de_pago = Dias_de_pago;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getHora() {
        return Hora;
    }

    public void setHora(String Hora) {
        this.Hora = Hora;
    }

    public Float getMonto_Abonado() {
        return Monto_Abonado;
    }

    public void setMonto_Abonado(Float Monto_Abonado) {
        this.Monto_Abonado = Monto_Abonado;
    }

    public int getId_factura() {
        return id_factura;
    }

    public void setId_factura(int id_factura) {
        this.id_factura = id_factura;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }
    
    
    
    


    
    


}