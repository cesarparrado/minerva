package Cartera;

import Personal.Persona;
import java.util.ArrayList;
//import java.util.*;

public class Cliente extends Persona {

    private String Monto_de_retencion;
    private String Tipo_de_retencion;

    public Cliente(String TipoIdentifiacion, String Identificacion, String Nombres, String Apellidos, String Celular, String Correo, String Direccion, String Monto_de_retencion/*, String Tipo_de_retencion*/) {
        super(TipoIdentifiacion, Identificacion, Nombres, Apellidos, Celular, Correo, Direccion);
        this.Monto_de_retencion = Monto_de_retencion;
        //this.Tipo_de_retencion = Tipo_de_retencion;
    }

    public Cliente() {
       
    }

    public String getMonto_de_retencion() {
        return Monto_de_retencion;
    }

    public void setMonto_de_retencion(String MontoDeRetencion) {
        this.Monto_de_retencion = MontoDeRetencion;
    }

    public String getTipo_de_retencion() {
        return Tipo_de_retencion;
    }

    public void setTipo_de_retencion(String Tipo_de_retencion) {
        this.Tipo_de_retencion = Tipo_de_retencion;
    }

    public Boolean tieneCreditos(ArrayList<Credito> creditos) {
        Boolean flag = false;
        for (int i = 0; i < creditos.size(); i++) {
            Credito c = creditos.get(i);
            if (c.getId_cliente().equals(this.getIdentificacion())) {
                switch (c.getEstado()) {
                    case "APROBADO":
                        flag = true;
                        break;
                    case "EN MORA":
                        flag = true;
                        break;
                    case "PAGADO":
                        flag = false;
                        break;
                    case "ANULADO":
                        flag = false;
                        break;
                    default:
                        flag = false;
                }
            }
        }
        return flag;
    }
    
    public Credito tieneCreditosDisponibles(ArrayList<Credito> creditos) {
        Credito cre = null;
        for (int i = 0; i < creditos.size(); i++) {
            Credito c = creditos.get(i);
            if (c.getId_cliente().equals(this.getIdentificacion())) {
                if(c.getEstado().equals("APROBADO") && c.getId_factura()==-1){
                    cre = c;
                    break;
                }
            }
        }
        return cre;
    }

}
