/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;

import java.util.*;

/**
 * 
 */
public class Marcas {

    /**
     * Default constructor
     */
    public Marcas() {
    }

    /**
     * 
     */
    private String Nombre;

    /**
     * 
     */
    private String Descripcion;

    /**
     * 
     */
    private String Identificacion;


    /**
     * @param Nombre 
     * @param Descripcion 
     * @param Identificacion String
     */
    public Marcas(String Nombre, String Descripcion,String Identificacion) {
        this.Nombre = Nombre;
        this.Descripcion = Descripcion;
        this.Identificacion = Identificacion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String Identificacion) {
        this.Identificacion = Identificacion;
    }
    
    

}