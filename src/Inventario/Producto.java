/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;

import java.util.*;

/**
 * 
 */
public class Producto {

    /**
     * 
     */
    private int Codigo;

    /**
     * 
     */
    private String Nombre;

    /**
     * 
     */
    private String Marca;

    /**
     * 
     */
    private String Categoria;

    /**
     * 
     */
    private int Stock;

    /**
     * 
     */
    private Float Iva;

    /**
     * 
     */
    private Float Precio;

    /**
     * 
     */
    private String Descripcion;

    /**
     * 
     */
    private String Ref1;

    /**
     * 
     */
    private String Ref2;

    /**
     * 
     */
    private String Ref3;

    /**
     * 
     */
    private String Ref4;

    public Producto(int Codigo, String Nombre, String Marca, String Categoria, int Stock, Float Iva, Float Precio, String Descripcion, String Ref1, String Ref2, String Ref3, String Ref4) {
        this.Codigo = Codigo;
        this.Nombre = Nombre;
        this.Marca = Marca;
        this.Categoria = Categoria;
        this.Stock = Stock;
        this.Iva = Iva;
        this.Precio = Precio;
        this.Descripcion = Descripcion;
        this.Ref1 = Ref1;
        this.Ref2 = Ref2;
        this.Ref3 = Ref3;
        this.Ref4 = Ref4;
    }
    
    public Producto(){
        
    }

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int Stock) {
        this.Stock = Stock;
    }

    public Float getIva() {
        return Iva;
    }

    public void setIva(Float Iva) {
        this.Iva = Iva;
    }

    public Float getPrecio() {
        return Precio;
    }

    public void setPrecio(Float Precio) {
        this.Precio = Precio;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getRef1() {
        return Ref1;
    }

    public void setRef1(String Ref1) {
        this.Ref1 = Ref1;
    }

    public String getRef2() {
        return Ref2;
    }

    public void setRef2(String Ref2) {
        this.Ref2 = Ref2;
    }

    public String getRef3() {
        return Ref3;
    }

    public void setRef3(String Ref3) {
        this.Ref3 = Ref3;
    }

    public String getRef4() {
        return Ref4;
    }

    public void setRef4(String Ref4) {
        this.Ref4 = Ref4;
    }
    
    
    
    
    

}