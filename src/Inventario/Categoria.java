/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;
import java.util.*;

/**
 * 
 */
public class Categoria {

    /**
     * Default constructor
     */
    public Categoria() {
    }

    /**
     * 
     */
    private String Nombre;

    /**
     * 
     */
    private String Descripcion;

    /**
     * 
     */
    private int Identificacion;


    /**
     * @param Nombre 
     * @param Descripcion
     */
    public Categoria(String Nombre, String Descripcion) {
        this.Nombre = Nombre;
        this.Descripcion =  Descripcion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public int getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(int Identificacion) {
        this.Identificacion = Identificacion;
    }
    
    

}