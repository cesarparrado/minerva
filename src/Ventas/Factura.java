
package Ventas;

import Cartera.Cliente;
import Inventario.Producto;
import Personal.Empleado;
import java.util.*;

/**
 * 
 */
public class Factura {


    private String Cliente;
    
    private String Empleado;
    
    private int id_de_la_venta;
    
    private String fecha;
    
    private String Forma_de_pago;

    private Float Rete_Fuente;

    private Float Rete_iva;

    private Float Rete_ica;

    private Float Subtotal;

    private Float IVA;

    private Float Descuento;

    private Float Total;

    private Boolean Anulada;

    public Factura(String Cliente, String Empleado, int id_de_la_venta, String fecha, String Forma_de_pago, Float Rete_Fuente, Float Rete_iva, Float Rete_ica, Float Subtotal, Float IVA, Float Descuento, Float Total, Boolean Estado) {
        this.Cliente = Cliente;
        this.Empleado = Empleado;
        this.id_de_la_venta = id_de_la_venta;
        this.fecha = fecha;
        this.Forma_de_pago = Forma_de_pago;
        this.Rete_Fuente = Rete_Fuente;
        this.Rete_iva = Rete_iva;
        this.Rete_ica = Rete_ica;
        this.Subtotal = Subtotal;
        this.IVA = IVA;
        this.Descuento = Descuento;
        this.Total = Total;
        this.Anulada = Estado;
    }

    public String getCliente() {
        return Cliente;
    }

    public void setCliente(String Cliente) {
        this.Cliente = Cliente;
    }

    public String getEmpleado() {
        return Empleado;
    }

    public void setEmpleado(String Empleado) {
        this.Empleado = Empleado;
    }

    public int getId_de_la_venta() {
        return id_de_la_venta;
    }

    public void setId_de_la_venta(int id_de_la_venta) {
        this.id_de_la_venta = id_de_la_venta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getForma_de_pago() {
        return Forma_de_pago;
    }

    public void setForma_de_pago(String Forma_de_pago) {
        this.Forma_de_pago = Forma_de_pago;
    }

    public Float getRete_Fuente() {
        return Rete_Fuente;
    }

    public void setRete_Fuente(Float Rete_Fuente) {
        this.Rete_Fuente = Rete_Fuente;
    }

    public Float getRete_iva() {
        return Rete_iva;
    }

    public void setRete_iva(Float Rete_iva) {
        this.Rete_iva = Rete_iva;
    }

    public Float getRete_ica() {
        return Rete_ica;
    }

    public void setRete_ica(Float Rete_ica) {
        this.Rete_ica = Rete_ica;
    }

    public Float getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(Float Subtotal) {
        this.Subtotal = Subtotal;
    }

    public Float getIVA() {
        return IVA;
    }

    public void setIVA(Float IVA) {
        this.IVA = IVA;
    }

    public Float getDescuento() {
        return Descuento;
    }

    public void setDescuento(Float Descuento) {
        this.Descuento = Descuento;
    }

    public Float getTotal() {
        return Total;
    }

    public void setTotal(Float Total) {
        this.Total = Total;
    }

    public Boolean getAnulada() {
        return Anulada;
    }

    public void setAnulada(Boolean Anulada) {
        this.Anulada = Anulada;
    }

    
    
    
    

}