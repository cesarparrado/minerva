/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Cartera.Cliente;
import Cartera.Credito;
import DB.CreditoDAO;
import static Interfaz.General.Escritorio;
import static Interfaz.General.clientes_array;
import static Interfaz.General.conexion;
import static Interfaz.General.creditos;
import java.awt.Dimension;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Yojan & Martha
 */
public class gestionarCartera extends javax.swing.JInternalFrame {

    CreditoDAO creditoDB = new CreditoDAO(conexion);

    public gestionarCartera() {
        initComponents();
        this.cargarTablas();
    }

    public void cargarTablas() {
        this.actualizarTablaAnular(null);
        this.actualizarTablaAsignarCredito(null);
        this.actualizarTablaCartera("todos", null);
    }

    public void actualizarTablaAsignarCredito(String id_cliente) {
        String[] titulos = {"ID", "NOMBRE", "DÍAS DE PLAZO"};
        String[] fila = new String[3];
        DefaultTableModel model = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int fila, int columna) {
                if (columna == 2) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        for (int i = 0; i < clientes_array.size(); i++) {
            Cliente e = clientes_array.get(i);
            Boolean tieneCreditos = e.tieneCreditos(creditos);
            fila[0] = e.getIdentificacion();
            fila[1] = e.getNombres();
            fila[2] = "";
            if (id_cliente == null && !tieneCreditos) {
                model.addRow(fila);
            } else {
                if (e.getIdentificacion().equals(id_cliente) && !tieneCreditos) {
                    model.addRow(fila);
                }
            }
        }
        this.jTable2.setModel(model);
    }

    public void actualizarTablaCartera(String tipo, String id_cl) {
        Float total = 0.0f;
        String[] titulos = {"CRÉD. N°", "CC", "NOMBRE", "DÍAS DE PLAZO", "FECHA", "MOROSO", "ESTADO", "FACTURA", "VALOR FACTURA"};
        String[] fila = new String[9];
        DefaultTableModel model = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int fila, int columna) {
                return false;
            }
        };
        ArrayList<Credito> creditos_aux = new ArrayList<Credito>();
        for (int k = 0; k < creditos.size(); k++) {
            Credito cre = creditos.get(k);
            switch (tipo) {
                case "todos":
                    creditos_aux.add(cre);
                    break;
                case "morosos":
                    if (cre.getEstado().equals("EN MORA")) {
                        creditos_aux.add(cre);
                    }
                    break;
                case "no morosos":
                    if (!cre.getEstado().equals("EN MORA")) {
                        creditos_aux.add(cre);
                    }
                    break;
                case "id":
                    if (id_cl != null) {
                        if (cre.getId_cliente().equals(id_cl)) {
                            creditos_aux.add(cre);
                        }
                    } else {
                        creditos_aux.add(cre);
                    }
                    break;
                default:
                    creditos_aux.add(cre);
            }
        }
        if (tipo.equals("id") && id_cl == null) {
            JOptionPane.showMessageDialog(this, "Digite un documento de algún cliente y luego de click en buscar.", "Info", JOptionPane.INFORMATION_MESSAGE);
        }
        for (int i = 0; i < creditos_aux.size(); i++) {
            Credito c = creditos_aux.get(i);
            fila[0] = "" + c.getID();
            Cliente cl = new Cliente();
            for (int j = 0; j < clientes_array.size(); j++) {
                Cliente cl_aux = clientes_array.get(j);
                if (cl_aux.getIdentificacion().equals(c.getId_cliente())) {
                    cl = cl_aux;
                }
            }
            fila[1] = cl.getIdentificacion();
            fila[2] = cl.getNombres();
            fila[3] = "" + c.getDias_de_pago();
            fila[4] = c.getFecha() + " " + c.getHora();
            fila[5] = "NO";
            if (c.getEstado().equals("EN MORA")) {
                fila[5] = "SÍ";
            }
            fila[6] = c.getEstado();
            fila[7] = "" + c.getId_factura();
            if (c.getId_factura() == -1) {
                fila[7] = "";
                fila[8] = "0.0";
            } else {
                fila[8] = "0.0";
            }
            model.addRow(fila);
            switch (fila[6]) {
                case "APROBADO":
                    total += Float.parseFloat(fila[8]);
                    break;
                case "EN MORA":
                    total += Float.parseFloat(fila[8]);
                    break;
                default:
                         ;
            }
        }
        this.jTable3.setModel(model);
        this.jLabel2.setText("$" + total);
    }

    public void actualizarTablaAnular(String id_cl) {
        Float total = 0.0f;
        String[] titulos = {"CRÉD. N°", "CC", "NOMBRE", "DÍAS DE PLAZO", "VALOR"};
        String[] fila = new String[5];
        DefaultTableModel model = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int fila, int columna) {
                return false;
            }
        };
        for (int i = 0; i < creditos.size(); i++) {
            Credito c = creditos.get(i);
            if (!c.getEstado().equals("ANULADO")) {
                fila[0] = "" + c.getID();
                Cliente cl = new Cliente();
                for (int j = 0; j < clientes_array.size(); j++) {
                    Cliente cl_aux = clientes_array.get(j);
                    if (cl_aux.getIdentificacion().equals(c.getId_cliente())) {
                        cl = cl_aux;
                    }
                }
                fila[1] = cl.getIdentificacion();
                fila[2] = cl.getNombres();
                fila[3] = "" + c.getDias_de_pago();
                fila[4] = "0.0";
                if (id_cl != null) {
                    if (c.getId_cliente().equals(id_cl)) {
                        model.addRow(fila);
                    }
                } else {
                    model.addRow(fila);
                }
            }
        }
        this.jTable6.setModel(model);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        listarca = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        buscadorlog = new javax.swing.JComboBox<>();
        numce = new javax.swing.JTextField();
        buscar1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        agregar1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        buscadorlog1 = new javax.swing.JComboBox<>();
        numce1 = new javax.swing.JTextField();
        buscar2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        buscadorlog4 = new javax.swing.JComboBox<>();
        numce6 = new javax.swing.JTextField();
        buscar5 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        eliminar5 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        buscadorlog3 = new javax.swing.JComboBox<>();
        numce3 = new javax.swing.JTextField();
        buscar4 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        numce4 = new javax.swing.JTextField();
        numce5 = new javax.swing.JTextField();
        pago = new javax.swing.JButton();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jButton2.setText("jButton2");

        setClosable(true);
        setTitle("CARTERA");

        jPanel2.setLayout(null);

        buscadorlog.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buscadorlog.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Identificacion" }));
        buscadorlog.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                buscadorlogAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        buscadorlog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscadorlogActionPerformed(evt);
            }
        });
        jPanel2.add(buscadorlog);
        buscadorlog.setBounds(36, 42, 178, 30);

        numce.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numceActionPerformed(evt);
            }
        });
        jPanel2.add(numce);
        numce.setBounds(260, 40, 116, 30);

        buscar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar.PNG"))); // NOI18N
        buscar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar1ActionPerformed(evt);
            }
        });
        jPanel2.add(buscar1);
        buscar1.setBounds(380, 40, 75, 31);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(111111), "Ariana Sofia Hernandez", null},
                { new Integer(123452), "Conhor David", null},
                { new Integer(123456), "Alejandro", null},
                { new Integer(145689), "Paulina", null}
            },
            new String [] {
                "ID", "NOMBRE", "DIAS DE PLAZO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
        }

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(36, 91, 640, 90);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Search.PNG"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3);
        jButton3.setBounds(460, 50, 20, 20);

        agregar1.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        agregar1.setText("GUARDAR");
        agregar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar1ActionPerformed(evt);
            }
        });
        jPanel2.add(agregar1);
        agregar1.setBounds(208, 201, 125, 32);

        listarca.addTab("Asignar credito", jPanel2);

        buscadorlog1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buscadorlog1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Listar todos los clientes", "Listar clientes Morosos", "Listar clientes No Morosos", "Buscar por Id" }));
        buscadorlog1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                buscadorlog1AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        buscadorlog1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscadorlog1ActionPerformed(evt);
            }
        });

        numce1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numce1ActionPerformed(evt);
            }
        });

        buscar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar.PNG"))); // NOI18N
        buscar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar2ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("DEUDA TOTAL ");

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setText("$30.000");

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(111111), "Ariana Sofia Hernandez",  new Integer(30), "02/05/2019", "NO",  new Integer(1111),  new Double(30000.0)},
                { new Integer(123452), "Conhor David",  new Integer(20), "03/04/2019", "SI",  new Integer(1125),  new Double(500000.0)},
                { new Integer(123456), "Alejandro",  new Integer(15), "05/08/2019", "NO",  new Integer(1123),  new Double(600000.0)},
                { new Integer(145689), "Paulina",  new Integer(10), "10/08/2016", "NO",  new Integer(1124),  new Double(215000.0)}
            },
            new String [] {
                "ID", "NOMBRE", "DIAS ", "FECHA", "MOROSO", "FACTURA", "V/FACTURA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane3.setViewportView(jTable3);
        if (jTable3.getColumnModel().getColumnCount() > 0) {
            jTable3.getColumnModel().getColumn(0).setResizable(false);
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(buscadorlog1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(75, 75, 75)
                        .addComponent(numce1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buscar2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 243, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 698, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addComponent(jLabel1)
                .addGap(34, 34, 34)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(buscar2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(numce1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(buscadorlog1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(119, 119, 119)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(135, Short.MAX_VALUE))
        );

        listarca.addTab("Listar cartera", jPanel3);

        jPanel6.setLayout(null);

        buscadorlog4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buscadorlog4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Buscar por Id" }));
        buscadorlog4.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                buscadorlog4AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        buscadorlog4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscadorlog4ActionPerformed(evt);
            }
        });
        jPanel6.add(buscadorlog4);
        buscadorlog4.setBounds(40, 80, 210, 30);

        numce6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numce6ActionPerformed(evt);
            }
        });
        jPanel6.add(numce6);
        numce6.setBounds(290, 80, 140, 30);

        buscar5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar.PNG"))); // NOI18N
        buscar5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar5ActionPerformed(evt);
            }
        });
        jPanel6.add(buscar5);
        buscar5.setBounds(430, 80, 70, 30);

        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(111111), "Ariana Sofia Hernandez",  new Integer(30)},
                { new Integer(123452), "Conhor David",  new Integer(15)},
                { new Integer(123456), "Alejandro",  new Integer(20)},
                { new Integer(145689), "Paulina", null}
            },
            new String [] {
                "ID", "NOMBRE", "DIAS DE PLAZO"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane6.setViewportView(jTable6);
        if (jTable6.getColumnModel().getColumnCount() > 0) {
            jTable6.getColumnModel().getColumn(0).setResizable(false);
        }

        jPanel6.add(jScrollPane6);
        jScrollPane6.setBounds(40, 160, 660, 90);

        eliminar5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        eliminar5.setText("CANCELAR CREDITO");
        eliminar5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminar5ActionPerformed(evt);
            }
        });
        jPanel6.add(eliminar5);
        eliminar5.setBounds(180, 290, 180, 30);

        listarca.addTab("Anular credito", jPanel6);

        jPanel4.setLayout(null);

        buscadorlog3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buscadorlog3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Buscar por Identificacion" }));
        buscadorlog3.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                buscadorlog3AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        buscadorlog3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscadorlog3ActionPerformed(evt);
            }
        });
        jPanel4.add(buscadorlog3);
        buscadorlog3.setBounds(9, 26, 178, 30);

        numce3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numce3ActionPerformed(evt);
            }
        });
        jPanel4.add(numce3);
        numce3.setBounds(262, 25, 116, 30);

        buscar4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar.PNG"))); // NOI18N
        jPanel4.add(buscar4);
        buscar4.setBounds(396, 25, 75, 31);

        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(111111), "Ariana Sofia Hernandez",  new Integer(30), "02/05/2019", "NO",  new Integer(1111),  new Double(30000.0)},
                { new Integer(123452), "Conhor David",  new Integer(20), "03/04/2019", "SI",  new Integer(1125),  new Double(500000.0)},
                { new Integer(123456), "Alejandro",  new Integer(15), "05/08/2019", "NO",  new Integer(1123),  new Double(600000.0)},
                { new Integer(145689), "Paulina",  new Integer(10), "10/08/2016", "NO",  new Integer(1124),  new Double(215000.0)}
            },
            new String [] {
                "ID", "NOMBRE", "DIAS DE PLAZO", "FECHA", "MOROSO", "FACTURA", "VALOR FACTURA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane5.setViewportView(jTable5);
        if (jTable5.getColumnModel().getColumnCount() > 0) {
            jTable5.getColumnModel().getColumn(0).setResizable(false);
        }

        jPanel4.add(jScrollPane5);
        jScrollPane5.setBounds(9, 86, 493, 91);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel4.setText("DEUDA TOTAL ");
        jPanel4.add(jLabel4);
        jLabel4.setBounds(212, 195, 118, 32);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel5.setText("$30.000");
        jPanel4.add(jLabel5);
        jLabel5.setBounds(348, 203, 67, 17);

        jRadioButton3.setText("ABONO ");
        jPanel4.add(jRadioButton3);
        jRadioButton3.setBounds(20, 260, 77, 23);

        jRadioButton4.setText("PAGO TOTAL DEUDA");
        jPanel4.add(jRadioButton4);
        jRadioButton4.setBounds(130, 260, 161, 23);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel6.setText("¿QUE DESEA HACER?");
        jPanel4.add(jLabel6);
        jLabel6.setBounds(30, 220, 168, 32);

        jLabel16.setText("_______________________________________________________________________________________");
        jPanel4.add(jLabel16);
        jLabel16.setBounds(20, 240, 550, 15);

        numce4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numce4ActionPerformed(evt);
            }
        });
        jPanel4.add(numce4);
        numce4.setBounds(20, 290, 96, 30);

        numce5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                numce5ActionPerformed(evt);
            }
        });
        jPanel4.add(numce5);
        numce5.setBounds(140, 290, 116, 30);

        pago.setText("REALIZAR PAGO");
        pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pagoActionPerformed(evt);
            }
        });
        jPanel4.add(pago);
        pago.setBounds(240, 340, 141, 30);

        listarca.addTab("Realizar pago", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(listarca)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(listarca)
        );

        getAccessibleContext().setAccessibleName("GESTIONAR CARTERA - MINERVA S.A.S");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void numce1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numce1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numce1ActionPerformed

    private void buscadorlog1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscadorlog1ActionPerformed
        int id_item = this.buscadorlog1.getSelectedIndex();
        switch (id_item) {
            case 0:
                this.actualizarTablaCartera("todos", null);
                break;
            case 1:
                this.actualizarTablaCartera("morosos", null);
                break;
            case 2:
                this.actualizarTablaCartera("no morosos", null);
                break;
            case 3:
                this.actualizarTablaCartera("id", null);
                break;
            default:
                this.actualizarTablaCartera("todos", null);
        }
    }//GEN-LAST:event_buscadorlog1ActionPerformed

    private void buscadorlog1AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_buscadorlog1AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog1AncestorAdded

    private void agregar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar1ActionPerformed
        for (int i = 0; i < jTable2.getRowCount(); i++) {
            String id_cliente = (String) jTable2.getValueAt(i, 0);
            String plazo = (String) jTable2.getValueAt(i, 2);
            try {
                int dias_plazo = Integer.parseInt(plazo);
                if (dias_plazo >= 1) {
                    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
                    DateFormat formatoHora = new SimpleDateFormat("hh:mm:ss");
                    Date date = Calendar.getInstance().getTime();
                    Credito c = new Credito(0, id_cliente, dias_plazo, formatoFecha.format(date), formatoHora.format(date), 0.0f, 0, "APROBADO");
                    creditoDB.insert(c);
                    creditos = creditoDB.list();
                    this.actualizarTablaAsignarCredito(null);
                }
            } catch (NumberFormatException e) {
                if (!plazo.equals("")) {
                    JOptionPane.showMessageDialog(this, "Para el cliente con documento N° " + id_cliente + " ingresaste un número de días invalido.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        this.cargarTablas();
        this.numce.setText("");
    }//GEN-LAST:event_agregar1ActionPerformed

    private void numceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numceActionPerformed

    private void buscadorlogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscadorlogActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlogActionPerformed

    private void buscadorlogAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_buscadorlogAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlogAncestorAdded

    private void buscadorlog3AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_buscadorlog3AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog3AncestorAdded

    private void buscadorlog3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscadorlog3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog3ActionPerformed

    private void numce3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numce3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numce3ActionPerformed

    private void numce4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numce4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numce4ActionPerformed

    private void numce5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numce5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numce5ActionPerformed

    private void buscadorlog4AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_buscadorlog4AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog4AncestorAdded

    private void buscadorlog4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscadorlog4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog4ActionPerformed

    private void numce6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_numce6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_numce6ActionPerformed

    private void buscar5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar5ActionPerformed
        String id_cliente = this.numce6.getText();
        this.actualizarTablaAnular(id_cliente);
    }//GEN-LAST:event_buscar5ActionPerformed

    private void pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pagoActionPerformed
        Float pago = 0.0f;
        formasdepago obj = new formasdepago("abono", pago, 1, null);
        General.Escritorio.add(obj);
        Dimension desktopSize = Escritorio.getSize();
        Dimension FrameSize = obj.getSize();
        obj.setLocation((desktopSize.width - FrameSize.width) / 2, (desktopSize.height - FrameSize.height) / 2);
        obj.toFront();
        obj.setVisible(true);

    }//GEN-LAST:event_pagoActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        gestionarCliente cli = new gestionarCliente();
        General.Escritorio.add(cli);
        Dimension desktopSize = Escritorio.getSize();
        Dimension FrameSize = cli.getSize();
        cli.setLocation((desktopSize.width - FrameSize.width) / 2, (desktopSize.height - FrameSize.height) / 2);
        cli.toFront();
        cli.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void buscar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar1ActionPerformed
        String id_cliente = this.numce.getText();
        this.actualizarTablaAsignarCredito(id_cliente);
    }//GEN-LAST:event_buscar1ActionPerformed

    private void buscar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar2ActionPerformed
        int id_item = this.buscadorlog1.getSelectedIndex();
        if (id_item == 3) {
            String id_cliente = this.numce1.getText();
            this.actualizarTablaCartera("id", id_cliente);
        }
    }//GEN-LAST:event_buscar2ActionPerformed

    private void eliminar5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminar5ActionPerformed
        try {
            int fila = this.jTable6.getSelectedRow();
            String id = (String) this.jTable6.getValueAt(fila, 0);
            int i = JOptionPane.showConfirmDialog(this, "¿Esta seguro dese anular este crédito?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if (i == 0) {
                creditoDB.anular(id);
                creditos = creditoDB.list();
                this.cargarTablas();
                this.numce6.setText("");
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "No ha seleccionado correctamente un crédito para anular.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_eliminar5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton agregar1;
    private javax.swing.JComboBox<String> buscadorlog;
    private javax.swing.JComboBox<String> buscadorlog1;
    private javax.swing.JComboBox<String> buscadorlog3;
    private javax.swing.JComboBox<String> buscadorlog4;
    private javax.swing.JButton buscar1;
    private javax.swing.JButton buscar2;
    private javax.swing.JButton buscar4;
    private javax.swing.JButton buscar5;
    private javax.swing.JButton eliminar5;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTable jTable5;
    private javax.swing.JTable jTable6;
    private javax.swing.JTabbedPane listarca;
    private javax.swing.JTextField numce;
    private javax.swing.JTextField numce1;
    private javax.swing.JTextField numce3;
    private javax.swing.JTextField numce4;
    private javax.swing.JTextField numce5;
    private javax.swing.JTextField numce6;
    private javax.swing.JButton pago;
    // End of variables declaration//GEN-END:variables
}
