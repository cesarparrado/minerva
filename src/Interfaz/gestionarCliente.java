/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Cartera.Cliente;
import DB.ClienteDAO;
import Filtros.FloatFilter;
import Filtros.IntFilter;
import Filtros.NitFilter;
import static Interfaz.General.clientes_array;
import static Interfaz.General.conexion;
import static Interfaz.gestionarVentas.cliente_venta_actual;
import static Interfaz.gestionarVentas.id_cliente_venta_actual;
import static Interfaz.gestionarVentas.nombre_cliente_venta_actual;
//import com.sun.javafx.geom.Matrix3f;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Yojan & Martha
 */
public class gestionarCliente extends javax.swing.JInternalFrame {

    int actualEditando = -1;
    ClienteDAO clienteDB = new ClienteDAO(conexion);

    public gestionarCliente() {
        initComponents();
        this.actualizarTabla();
        this.cargarFiltros();
    }

    public void cargarFiltros() {
        //Filtros celular
        PlainDocument doc = (PlainDocument) this.txtCelular.getDocument();
        doc.setDocumentFilter(new IntFilter());
        PlainDocument doc1 = (PlainDocument) this.txteditarcelular.getDocument();
        doc1.setDocumentFilter(new IntFilter());
        //Filtros Monto Retencion
        PlainDocument doc2 = (PlainDocument) this.txtMontoRetencion.getDocument();
        doc2.setDocumentFilter(new FloatFilter());
        PlainDocument doc3 = (PlainDocument) this.txteditarMontoRetencion.getDocument();
        doc3.setDocumentFilter(new FloatFilter());
        //Filtros Identificacion
        PlainDocument doc4 = (PlainDocument) this.txtNumIdentificacion.getDocument();
        doc4.setDocumentFilter(new NitFilter());
        PlainDocument doc5 = (PlainDocument) this.txteditarNumdentificacion.getDocument();
        doc5.setDocumentFilter(new NitFilter());
    }

    public void actualizarTabla() {
        String[] titulos = {"ID", "NOMBRE", "DIRECCIÓN", "CELULAR", "EMAIL"};
        String[] fila = new String[5];
        DefaultTableModel model = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int fila, int columna) {
                return false;
            }
        };
        for (int i = 0; i < clientes_array.size(); i++) {
            Cliente e = clientes_array.get(i);
            fila[0] = e.getIdentificacion();
            fila[1] = e.getNombres();
            fila[2] = e.getDireccion();
            fila[3] = e.getCelular();
            fila[4] = e.getCorreo();
            model.addRow(fila);
        }
        this.jTableClientes.setModel(model);
    }

    public void editarItem(String id) {
        this.actualEditando = -1;
        for (int i = 0; i < clientes_array.size(); i++) {
            Cliente e = clientes_array.get(i);
            if (e.getIdentificacion().equals(id)) {
                this.actualEditando = i;
                break;
            }
        }
    }

    public void eliminarItem(String id) {
        for (int i = 0; i < clientes_array.size(); i++) {
            Cliente e = clientes_array.get(i);
            if (e.getIdentificacion().equals(id)) {
                int status = clienteDB.delete(e.getIdentificacion());
                if (status == 1) {
                    clientes_array.remove(i);
                }
                break;
            }
        }
    }

    public void limpiarCrear() {
        this.txtNombres.setText("");
        this.txtApellidos.setText("");
        this.txtNumIdentificacion.setText("");
        this.buscadorlog2.setSelectedIndex(0);
        this.txtEmail.setText("");
        this.txtDireccion.setText("");
        this.txtCelular.setText("");
        this.txtMontoRetencion.setText("");
    }

    public void limpiarEditar() {
        this.txteditarNombres.setText("");
        this.txteditarApellidos.setText("");
        this.txteditarNumdentificacion.setText("");
        this.buscadorlog3.setSelectedIndex(0);
        this.txteditarEmail.setText("");
        this.txteditarDireccion.setText("");
        this.txteditarcelular.setText("");
        this.txteditarMontoRetencion.setText("");
        this.txtBuscarenEditar.setText("");
        this.actualEditando = -1;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        clientes = new javax.swing.JPanel();
        BUSCADOR3 = new javax.swing.JComboBox<>();
        btnBuscar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableClientes = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        eliminarCliente = new javax.swing.JButton();
        añadirFactura = new javax.swing.JButton();
        btnMostrartodos = new javax.swing.JButton();
        txtBuscar = new javax.swing.JTextField();
        crearClientes = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txtNombres = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        txtNumIdentificacion = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        txtMontoRetencion = new javax.swing.JTextField();
        jRadioButton7 = new javax.swing.JRadioButton();
        jRadioButton8 = new javax.swing.JRadioButton();
        jRadioButton9 = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        guardarCli = new javax.swing.JButton();
        buscadorlog2 = new javax.swing.JComboBox<>();
        editarCliente = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txteditarNombres = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        txteditarApellidos = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        txteditarDireccion = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        txteditarcelular = new javax.swing.JTextField();
        txteditarNumdentificacion = new javax.swing.JTextField();
        buscadorlog3 = new javax.swing.JComboBox<>();
        txteditarEmail = new javax.swing.JTextField();
        jLabel39 = new javax.swing.JLabel();
        jRadioButton10 = new javax.swing.JRadioButton();
        jRadioButton11 = new javax.swing.JRadioButton();
        jRadioButton12 = new javax.swing.JRadioButton();
        txteditarMontoRetencion = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        GuardareditarCli = new javax.swing.JButton();
        BUSCADOR5 = new javax.swing.JComboBox<>();
        txtBuscarenEditar = new javax.swing.JTextField();
        btnBuscarenEditar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        setClosable(true);
        setTitle("CLIENTES");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        clientes.setLayout(null);

        BUSCADOR3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BUSCADOR3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Numero Identificacion", "Nombres" }));
        BUSCADOR3.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                BUSCADOR3AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        BUSCADOR3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BUSCADOR3ActionPerformed(evt);
            }
        });
        clientes.add(BUSCADOR3);
        BUSCADOR3.setBounds(160, 100, 150, 30);

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar.PNG"))); // NOI18N
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        clientes.add(btnBuscar);
        btnBuscar.setBounds(450, 100, 70, 30);

        jTableClientes.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTableClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "IDENTIFICACION", "NOMBRES", "DIRECCION", "CELULAR", "EMAIL"
            }
        ));
        jTableClientes.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jScrollPane3.setViewportView(jTableClientes);

        clientes.add(jScrollPane3);
        jScrollPane3.setBounds(20, 150, 500, 120);

        jLabel3.setText("Buscar por:");
        clientes.add(jLabel3);
        jLabel3.setBounds(160, 80, 90, 15);

        eliminarCliente.setText("Eliminar");
        eliminarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarClienteActionPerformed(evt);
            }
        });
        clientes.add(eliminarCliente);
        eliminarCliente.setBounds(130, 280, 90, 30);

        añadirFactura.setText("Añadir a Factura");
        añadirFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                añadirFacturaActionPerformed(evt);
            }
        });
        clientes.add(añadirFactura);
        añadirFactura.setBounds(250, 280, 120, 30);

        btnMostrartodos.setText("Mostrar Todo");
        btnMostrartodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrartodosActionPerformed(evt);
            }
        });
        clientes.add(btnMostrartodos);
        btnMostrartodos.setBounds(30, 100, 110, 30);
        clientes.add(txtBuscar);
        txtBuscar.setBounds(339, 100, 110, 30);

        jTabbedPane1.addTab("Clientes", clientes);

        crearClientes.setLayout(null);

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("DATOS DEL  CLIENTE");
        crearClientes.add(jLabel5);
        jLabel5.setBounds(190, 0, 150, 28);

        jLabel24.setText("_______________________________________________________________________________________");
        crearClientes.add(jLabel24);
        jLabel24.setBounds(10, 10, 590, 15);

        jLabel25.setText("Nombres:");
        crearClientes.add(jLabel25);
        jLabel25.setBounds(20, 30, 80, 15);

        jLabel26.setText("Apellidos:");
        crearClientes.add(jLabel26);
        jLabel26.setBounds(180, 30, 70, 20);

        jLabel27.setText("Direccion:");
        crearClientes.add(jLabel27);
        jLabel27.setBounds(350, 30, 70, 15);

        txtNombres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombresActionPerformed(evt);
            }
        });
        crearClientes.add(txtNombres);
        txtNombres.setBounds(20, 50, 120, 30);

        txtApellidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidosActionPerformed(evt);
            }
        });
        crearClientes.add(txtApellidos);
        txtApellidos.setBounds(180, 50, 120, 30);

        txtDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDireccionActionPerformed(evt);
            }
        });
        crearClientes.add(txtDireccion);
        txtDireccion.setBounds(350, 50, 151, 30);

        jLabel28.setText("Tipo Identificacion:");
        crearClientes.add(jLabel28);
        jLabel28.setBounds(20, 90, 140, 15);

        jLabel8.setText("Num Identificacion:");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        crearClientes.add(jLabel8);
        jLabel8.setBounds(180, 90, 120, 15);

        jLabel29.setText("Celular:");
        crearClientes.add(jLabel29);
        jLabel29.setBounds(350, 90, 60, 15);

        txtCelular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCelularActionPerformed(evt);
            }
        });
        crearClientes.add(txtCelular);
        txtCelular.setBounds(350, 110, 150, 30);

        txtNumIdentificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNumIdentificacionActionPerformed(evt);
            }
        });
        crearClientes.add(txtNumIdentificacion);
        txtNumIdentificacion.setBounds(180, 110, 151, 30);
        crearClientes.add(txtEmail);
        txtEmail.setBounds(20, 170, 150, 30);

        jLabel30.setText("E-mail:");
        crearClientes.add(jLabel30);
        jLabel30.setBounds(20, 150, 70, 20);

        txtMontoRetencion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMontoRetencionActionPerformed(evt);
            }
        });
        crearClientes.add(txtMontoRetencion);
        txtMontoRetencion.setBounds(30, 280, 151, 30);

        buttonGroup2.add(jRadioButton7);
        jRadioButton7.setText("Rte Fuente");
        jRadioButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton7ActionPerformed(evt);
            }
        });
        crearClientes.add(jRadioButton7);
        jRadioButton7.setBounds(20, 230, 89, 23);

        buttonGroup2.add(jRadioButton8);
        jRadioButton8.setText("Rte Iva");
        jRadioButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton8ActionPerformed(evt);
            }
        });
        crearClientes.add(jRadioButton8);
        jRadioButton8.setBounds(120, 230, 67, 23);

        buttonGroup2.add(jRadioButton9);
        jRadioButton9.setText("Rete Ica");
        jRadioButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton9ActionPerformed(evt);
            }
        });
        crearClientes.add(jRadioButton9);
        jRadioButton9.setBounds(200, 230, 69, 23);

        jLabel6.setText("Monto de retencion: ");
        crearClientes.add(jLabel6);
        jLabel6.setBounds(30, 260, 110, 20);

        guardarCli.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        guardarCli.setText("GUARDAR");
        guardarCli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarCliActionPerformed(evt);
            }
        });
        crearClientes.add(guardarCli);
        guardarCli.setBounds(200, 330, 100, 30);

        buscadorlog2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buscadorlog2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CC", "NIT", "CE" }));
        buscadorlog2.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                buscadorlog2AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        buscadorlog2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscadorlog2ActionPerformed(evt);
            }
        });
        crearClientes.add(buscadorlog2);
        buscadorlog2.setBounds(20, 110, 80, 30);

        jTabbedPane1.addTab("Crear Cliente", crearClientes);

        editarCliente.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel31.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel31.setText("DATOS DEL  CLIENTE");
        editarCliente.add(jLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, 150, 40));

        jLabel32.setText("_______________________________________________________________________________________");
        editarCliente.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        txteditarNombres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txteditarNombresActionPerformed(evt);
            }
        });
        editarCliente.add(txteditarNombres, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 120, 30));

        jLabel33.setText("Nombres:");
        editarCliente.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 80, -1));

        jLabel34.setText("Apellidos:");
        editarCliente.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 110, 70, 20));

        txteditarApellidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txteditarApellidosActionPerformed(evt);
            }
        });
        editarCliente.add(txteditarApellidos, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 130, 120, 30));

        jLabel35.setText("Direccion:");
        editarCliente.add(jLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 110, 70, -1));

        txteditarDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txteditarDireccionActionPerformed(evt);
            }
        });
        editarCliente.add(txteditarDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 130, 151, 30));

        jLabel36.setText("Tipo Identificacion:");
        editarCliente.add(jLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 140, -1));

        jLabel37.setText("Num Identificacion:");
        jLabel37.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        editarCliente.add(jLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 170, 100, -1));

        jLabel38.setText("Celular:");
        editarCliente.add(jLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 170, -1, -1));

        txteditarcelular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txteditarcelularActionPerformed(evt);
            }
        });
        editarCliente.add(txteditarcelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 190, 150, 30));

        txteditarNumdentificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txteditarNumdentificacionActionPerformed(evt);
            }
        });
        editarCliente.add(txteditarNumdentificacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 190, 151, 30));

        buscadorlog3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buscadorlog3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CC", "NIT", "CE" }));
        buscadorlog3.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                buscadorlog3AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        buscadorlog3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscadorlog3ActionPerformed(evt);
            }
        });
        editarCliente.add(buscadorlog3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 80, 30));
        editarCliente.add(txteditarEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 150, 30));

        jLabel39.setText("E-mail:");
        editarCliente.add(jLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, 70, 20));

        buttonGroup1.add(jRadioButton10);
        jRadioButton10.setText("Rte Fuente");
        jRadioButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton10ActionPerformed(evt);
            }
        });
        editarCliente.add(jRadioButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 260, 89, -1));

        buttonGroup1.add(jRadioButton11);
        jRadioButton11.setText("Rte Iva");
        jRadioButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton11ActionPerformed(evt);
            }
        });
        editarCliente.add(jRadioButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 260, 67, -1));

        buttonGroup1.add(jRadioButton12);
        jRadioButton12.setText("Rete Ica");
        jRadioButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton12ActionPerformed(evt);
            }
        });
        editarCliente.add(jRadioButton12, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 260, 69, -1));

        txteditarMontoRetencion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txteditarMontoRetencionActionPerformed(evt);
            }
        });
        editarCliente.add(txteditarMontoRetencion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, 151, 30));

        jLabel40.setText("Monto de retencion: ");
        editarCliente.add(jLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 110, 20));

        GuardareditarCli.setText("Guardar");
        GuardareditarCli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GuardareditarCliActionPerformed(evt);
            }
        });
        editarCliente.add(GuardareditarCli, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 310, -1, 30));

        BUSCADOR5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        BUSCADOR5.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Numero Identificacion" }));
        BUSCADOR5.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                BUSCADOR5AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        BUSCADOR5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BUSCADOR5ActionPerformed(evt);
            }
        });
        editarCliente.add(BUSCADOR5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, -1, 30));

        txtBuscarenEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBuscarenEditarActionPerformed(evt);
            }
        });
        editarCliente.add(txtBuscarenEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 30, 110, 30));

        btnBuscarenEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar.PNG"))); // NOI18N
        btnBuscarenEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarenEditarActionPerformed(evt);
            }
        });
        editarCliente.add(btnBuscarenEditar, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 30, 70, 30));

        jLabel4.setText("Buscar por:");
        editarCliente.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 10, -1, -1));

        jTabbedPane1.addTab("Editar Cliente", editarCliente);

        getContentPane().add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 545, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BUSCADOR3AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_BUSCADOR3AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_BUSCADOR3AncestorAdded

    private void BUSCADOR3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BUSCADOR3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BUSCADOR3ActionPerformed

    private void txtNombresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombresActionPerformed

    private void txtApellidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidosActionPerformed

    private void txtDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDireccionActionPerformed

    private void txtCelularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCelularActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCelularActionPerformed

    private void txtNumIdentificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNumIdentificacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNumIdentificacionActionPerformed

    private void txtMontoRetencionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMontoRetencionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMontoRetencionActionPerformed

    private void jRadioButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton7ActionPerformed

    private void jRadioButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton8ActionPerformed

    private void jRadioButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton9ActionPerformed

    private void guardarCliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarCliActionPerformed
        Cliente e = new Cliente((String) this.buscadorlog2.getSelectedItem(), this.txtNumIdentificacion.getText(), this.txtNombres.getText(), this.txtApellidos.getText(), this.txtCelular.getText(), this.txtEmail.getText(), this.txtDireccion.getText(), this.txtMontoRetencion.getText());
        int status = clienteDB.insert(e);
        if (status == 1) {
            JOptionPane.showMessageDialog(this, "Se guardo con exito");
            clientes_array.add(e);
            this.actualizarTabla();
        } else {
            JOptionPane.showMessageDialog(this, "Ha ocurrido un error al realizar el registro en la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
        }
        this.jTabbedPane1.setSelectedIndex(0);
        this.limpiarCrear();
    }//GEN-LAST:event_guardarCliActionPerformed

    private void buscadorlog2AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_buscadorlog2AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog2AncestorAdded

    private void buscadorlog2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscadorlog2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog2ActionPerformed

    private void txteditarNombresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txteditarNombresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txteditarNombresActionPerformed

    private void txteditarApellidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txteditarApellidosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txteditarApellidosActionPerformed

    private void txteditarDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txteditarDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txteditarDireccionActionPerformed

    private void txteditarcelularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txteditarcelularActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txteditarcelularActionPerformed

    private void txteditarNumdentificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txteditarNumdentificacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txteditarNumdentificacionActionPerformed

    private void buscadorlog3AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_buscadorlog3AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog3AncestorAdded

    private void buscadorlog3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscadorlog3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buscadorlog3ActionPerformed

    private void jRadioButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton10ActionPerformed

    private void jRadioButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton11ActionPerformed

    private void jRadioButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton12ActionPerformed

    private void txteditarMontoRetencionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txteditarMontoRetencionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txteditarMontoRetencionActionPerformed

    private void GuardareditarCliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GuardareditarCliActionPerformed
        if (this.actualEditando != -1) {
            Cliente e = new Cliente((String) this.buscadorlog3.getSelectedItem(), this.txteditarNumdentificacion.getText(), this.txteditarNombres.getText(), this.txteditarApellidos.getText(), this.txteditarcelular.getText(), this.txteditarEmail.getText(), this.txteditarDireccion.getText(), this.txteditarMontoRetencion.getText());
            Cliente ce = clientes_array.get(this.actualEditando);
            int status = clienteDB.update(ce.getIdentificacion(), e);
            if (status == 1) {
                clientes_array.set(this.actualEditando, e);
                this.actualizarTabla();
                JOptionPane.showMessageDialog(this, "Se guardo con exito");
            } else {
                JOptionPane.showMessageDialog(this, "Ha ocurrido un error al actualizar el registro en la base de datos.", "Error", JOptionPane.ERROR_MESSAGE);
            }
            this.jTabbedPane1.setSelectedIndex(0);
            this.limpiarEditar();
        } else {
            JOptionPane.showMessageDialog(this, "Actualmente usted no esta editando ningun cliente.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_GuardareditarCliActionPerformed

    private void eliminarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarClienteActionPerformed
        try {
            int fila = this.jTableClientes.getSelectedRow();
            String id = (String) this.jTableClientes.getValueAt(fila, 0);
            int i = JOptionPane.showConfirmDialog(this, "¿Esta seguro de eliminar a este cliente?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if (i == 0) {
                this.eliminarItem(id);
                this.actualizarTabla();
            }
        } catch (Exception ex) {
            this.jTabbedPane1.setSelectedIndex(0);
            JOptionPane.showMessageDialog(this, "No ha seleccionado correctamente un cliente para eliminar.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_eliminarClienteActionPerformed

    private void añadirFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_añadirFacturaActionPerformed
        int i = JOptionPane.showConfirmDialog(this, "¿Esta seguro desea agregar este cliente a la venta?", "Advertencia", JOptionPane.YES_NO_OPTION);
        if (i == 0) {
            try {
                int fila = this.jTableClientes.getSelectedRow();
                cliente_venta_actual = clientes_array.get(fila);
                nombre_cliente_venta_actual.setText(cliente_venta_actual.getNombres());
                id_cliente_venta_actual.setText(cliente_venta_actual.getIdentificacion());
                dispose();
            } catch (Exception ex) {
                this.jTabbedPane1.setSelectedIndex(0);
                JOptionPane.showMessageDialog(null, "No ha seleccionado correctamente un cliente para agregar.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_añadirFacturaActionPerformed

    private void btnMostrartodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrartodosActionPerformed

    }//GEN-LAST:event_btnMostrartodosActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

    }//GEN-LAST:event_btnBuscarActionPerformed

    private void BUSCADOR5AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_BUSCADOR5AncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_BUSCADOR5AncestorAdded

    private void BUSCADOR5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BUSCADOR5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BUSCADOR5ActionPerformed

    private void btnBuscarenEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarenEditarActionPerformed
        String id = this.txtBuscarenEditar.getText();
        this.editarItem(id);
        if (this.actualEditando >= 0) {
            Cliente e = clientes_array.get(this.actualEditando);
            this.txteditarNombres.setText(e.getNombres());
            this.txteditarApellidos.setText(e.getApellidos());
            this.txteditarNumdentificacion.setText(e.getIdentificacion());
            for (int i = 0; i < this.buscadorlog3.getItemCount(); i++) {
                String item = this.buscadorlog3.getItemAt(i);
                if (e.getTipoIdentificacion().equalsIgnoreCase(item)) {
                    buscadorlog3.setSelectedIndex(i);
                    break;
                }
            }
            this.txteditarEmail.setText(e.getCorreo());
            this.txteditarDireccion.setText(e.getDireccion());
            this.txteditarcelular.setText(e.getCelular());
            this.txteditarMontoRetencion.setText(e.getMonto_de_retencion());
        } else {
            JOptionPane.showMessageDialog(null, "No se ha encontrado ningún un cliente para editar.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnBuscarenEditarActionPerformed

    private void txtBuscarenEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBuscarenEditarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscarenEditarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> BUSCADOR3;
    private javax.swing.JComboBox<String> BUSCADOR5;
    private javax.swing.JButton GuardareditarCli;
    private javax.swing.JButton añadirFactura;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnBuscarenEditar;
    private javax.swing.JButton btnMostrartodos;
    private javax.swing.JComboBox<String> buscadorlog2;
    private javax.swing.JComboBox<String> buscadorlog3;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JPanel clientes;
    private javax.swing.JPanel crearClientes;
    private javax.swing.JPanel editarCliente;
    private javax.swing.JButton eliminarCliente;
    private javax.swing.JButton guardarCli;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JRadioButton jRadioButton10;
    private javax.swing.JRadioButton jRadioButton11;
    private javax.swing.JRadioButton jRadioButton12;
    private javax.swing.JRadioButton jRadioButton7;
    private javax.swing.JRadioButton jRadioButton8;
    private javax.swing.JRadioButton jRadioButton9;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableClientes;
    private javax.swing.JTextField txtApellidos;
    public static javax.swing.JTextField txtBuscar;
    public javax.swing.JTextField txtBuscarenEditar;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtMontoRetencion;
    private javax.swing.JTextField txtNombres;
    private javax.swing.JTextField txtNumIdentificacion;
    private javax.swing.JTextField txteditarApellidos;
    private javax.swing.JTextField txteditarDireccion;
    private javax.swing.JTextField txteditarEmail;
    private javax.swing.JTextField txteditarMontoRetencion;
    private javax.swing.JTextField txteditarNombres;
    private javax.swing.JTextField txteditarNumdentificacion;
    private javax.swing.JTextField txteditarcelular;
    // End of variables declaration//GEN-END:variables
}
