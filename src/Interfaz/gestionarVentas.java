/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Cartera.Cliente;
import DB.FacturaDAO;
import static Interfaz.General.Escritorio;
import static Interfaz.General.conexion;
import static Interfaz.General.contadorVentas;
import static Interfaz.General.empleado_actual;
import static Interfaz.General.facturas_array;
import Inventario.Producto;
import Ventas.Factura;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import static java.lang.reflect.Array.set;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Yojan & Martha
 */
public class gestionarVentas extends javax.swing.JInternalFrame {

    public static ArrayList<Producto> productos_venta = new ArrayList<Producto>();
    public static Cliente cliente_venta_actual;
    Float total_iva = 0.0f;
    Float total_descuentos = 0.0f;
    Float total_venta = 0.0f;
    FacturaDAO facturaDB =  new FacturaDAO(conexion);

    public gestionarVentas() {
        initComponents();
        productos_venta = new ArrayList<Producto>();
        String[] titulos = {"CÓDIGO", "NOMBRE", "VALOR UNITARIO", "DESCUENTO (%)", "IVA(%)", "CANTIDAD", "TOTAL"};
        DefaultTableModel model = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int fila, int columna) {
                if (columna == 3 || columna == 5) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        this.tablaProductosVenta.setModel(model);
        this.tablaProductosVenta.getDefaultEditor(String.class).addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingCanceled(ChangeEvent e) {
                System.out.println("editingCanceled");
            }

            @Override
            public void editingStopped(ChangeEvent e) {
                recalcularValores();
            }
        });
        this.jTextField2.setText(empleado_actual.getNombres());
        this.jTextField1.setText(empleado_actual.getIdentificacion());
        this.actualizarTabla();
    }

    public void recalcularValores() {
        total_iva = 0.0f;
        total_descuentos = 0.0f;
        total_venta = 0.0f;
        DefaultTableModel model = (DefaultTableModel) this.tablaProductosVenta.getModel();
        for (int i = 0; i < this.tablaProductosVenta.getRowCount(); i++) {
            Float valor_unitario = Float.parseFloat((String) this.tablaProductosVenta.getValueAt(i, 2));
            Float cantidad = Float.parseFloat((String) this.tablaProductosVenta.getValueAt(i, 5));
            Float descuento = Float.parseFloat((String) this.tablaProductosVenta.getValueAt(i, 3));
            Float iva = Float.parseFloat((String) this.tablaProductosVenta.getValueAt(i, 4));
            if (descuento > 10.0) {
                model.setValueAt("10.0", i, 3);
                JOptionPane.showMessageDialog(null, "No puede tener más del 10% del descuento.", "Error", JOptionPane.ERROR_MESSAGE);
            }
            Float total = valor_unitario * cantidad;
            total_iva += (total * (iva / 100));
            total_descuentos += (total * (descuento / 100));
            total_venta += total;
            model.setValueAt(Float.toString(total), i, 6);
        }
        this.jTextField6.setText(Float.toString(total_venta - total_iva - total_descuentos));
        this.jTextField7.setText(Float.toString(total_descuentos));
        this.jTextField8.setText(Float.toString(total_iva));
        this.jTextField12.setText(Float.toString(total_venta - total_descuentos));
    }

    public void actualizarTabla() {
        String[] titulos = {"NÚM. FACTURA", "IDENTIFICACIÓN", "ANULADA", "FECHA", "VALOR FACTURA"};
        String[] fila = new String[5];
        DefaultTableModel model = new DefaultTableModel(null, titulos) {
            @Override
            public boolean isCellEditable(int fila, int columna) {
                return false;
            }
        };
        for (int i = 0; i < facturas_array.size(); i++) {
            Factura f = facturas_array.get(i);
            fila[0] = Integer.toString(f.getId_de_la_venta());
            fila[1] = f.getCliente();
            fila[2] = (f.getAnulada())?"SÍ":"NO";
            fila[3] = f.getFecha().toString();
            fila[4] = Float.toString(f.getTotal());
            model.addRow(fila);
        }
        this.jTable2.setModel(model);
    }

    public void exportarPDF(String ruta) throws FileNotFoundException, DocumentException {
        //225 , 648
        Rectangle medidas = new Rectangle(448, 648);
        Document documento = new Document(medidas);
        documento.top(0);
        FileOutputStream ficheroPdf = new FileOutputStream(ruta + ".pdf");
        PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);
        documento.open();
        String[] encabezados = {
            "MINERVA 2.0",
            "NIT: 800900950-5",
            "TEL: 6676862",
            "KR 30 N° 20-61 ALBORADA",
            "VILLAVICENCIO",
            "----------------------------------------"
        };

        for (int i = 0; i < encabezados.length; i++) {
            Paragraph preface = new Paragraph(encabezados[i]);
            preface.setAlignment(Element.ALIGN_CENTER);
            documento.add(preface);
        }

        String[] info = {
            "FACTURA N° 1", "FECHA 28/01/2019 HORA 6:35 PM",
            "VENDEDOR: " + empleado_actual.getNombres().toUpperCase() + " " + empleado_actual.getApellidos().toUpperCase(),
            cliente_venta_actual.getTipoIdentificacion() + ": " + cliente_venta_actual.getIdentificacion(),
            "NOMBRE: " + cliente_venta_actual.getNombres().toUpperCase() + " " + cliente_venta_actual.getApellidos().toUpperCase(),
            "DIR.: " + cliente_venta_actual.getDireccion().toUpperCase(),
            "CEL: " + cliente_venta_actual.getCelular(),
            "EMAIL: " + cliente_venta_actual.getCorreo(),
            "----------------------------------------"
        };

        for (int i = 0; i < info.length; i++) {
            Paragraph preface = new Paragraph(info[i]);
            preface.setAlignment(Element.ALIGN_LEFT);
            documento.add(preface);
        }

        PdfPTable tabla = new PdfPTable(tablaProductosVenta.getColumnCount());
        float[] medidaCeldas = new float[tablaProductosVenta.getColumnCount()];
        for (int i = 0; i <= tablaProductosVenta.getColumnCount() - 1; i++) {
            medidaCeldas[i] = 1.0f;
        }
        tabla.setWidths(medidaCeldas);
        for (int i = 0; i < tablaProductosVenta.getColumnCount(); i++) {
            PdfPCell cell = new PdfPCell(new Phrase(tablaProductosVenta.getColumnName(i).toString()));
            cell.setUseVariableBorders(true);
            cell.setBorderColorBottom(BaseColor.WHITE);
            cell.setBorderColorTop(BaseColor.WHITE);
            cell.setBorderColorLeft(BaseColor.WHITE);
            cell.setBorderColorRight(BaseColor.WHITE);
            tabla.addCell(cell);
        }
        for (int i = 0; i < tablaProductosVenta.getRowCount(); i++) {
            for (int j = 0; j < tablaProductosVenta.getColumnCount(); j++) {
                PdfPCell cell = new PdfPCell(new Phrase((String) tablaProductosVenta.getValueAt(i, j)));
                cell.setUseVariableBorders(true);
                cell.setBorderColorBottom(BaseColor.WHITE);
                cell.setBorderColorTop(BaseColor.WHITE);
                cell.setBorderColorLeft(BaseColor.WHITE);
                cell.setBorderColorRight(BaseColor.WHITE);
                tabla.addCell(cell);
            }
        }
        documento.add(tabla);

        Paragraph preface3 = new Paragraph("---------------");
        preface3.setAlignment(Element.ALIGN_CENTER);
        documento.add(preface3);

        String[] nombreCampos = {"Subtotal", "Iva", "Reteica", "Reteiva", "Retefuente", "Descuentos", "Total"};
        JTextField[] campos = {jTextField6, jTextField8, jTextField11, jTextField10, jTextField9, jTextField7, jTextField12};
        PdfPTable tabla1 = new PdfPTable(2);
        float[] medidaCeldas1 = {1.0f, 1.0f};
        tabla1.setWidths(medidaCeldas1);
        for (int i = 0; i < campos.length; i++) {
            PdfPCell cell = new PdfPCell(new Phrase(nombreCampos[i]));
            cell.setUseVariableBorders(true);
            cell.setBorderColorBottom(BaseColor.WHITE);
            cell.setBorderColorTop(BaseColor.WHITE);
            cell.setBorderColorLeft(BaseColor.WHITE);
            cell.setBorderColorRight(BaseColor.WHITE);
            tabla1.addCell(cell);
            PdfPCell cell1 = new PdfPCell(new Phrase(campos[i].getText()));
            cell1.setUseVariableBorders(true);
            cell1.setBorderColorBottom(BaseColor.WHITE);
            cell1.setBorderColorTop(BaseColor.WHITE);
            cell1.setBorderColorLeft(BaseColor.WHITE);
            cell1.setBorderColorRight(BaseColor.WHITE);
            tabla1.addCell(cell1);
        }
        documento.add(tabla1);
        documento.close();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProductosVenta = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        eliminar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        nombre_cliente_venta_actual = new javax.swing.JTextField();
        id_cliente_venta_actual = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jTextField12 = new javax.swing.JTextField();
        pago = new javax.swing.JButton();
        printBoton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jTextField13 = new javax.swing.JTextField();
        jButton4 = new javax.swing.JButton();
        anularFactura = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();

        setClosable(true);
        setTitle("GESTIONAR VENTAS - MINERVA S.A.S");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Nombres: ");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, -1, -1));

        jLabel2.setText("Identificacion:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 30, 100, -1));

        jLabel3.setText("Cargo:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 30, 60, -1));

        jLabel4.setText("Nombres:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 110, 110, -1));

        jLabel5.setText("Identificacion:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 110, 110, -1));

        tablaProductosVenta.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tablaProductosVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Codigo ", "Nombre", "Valor Unitario", "Cantidad", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Double.class, java.lang.Integer.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tablaProductosVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaProductosVentaKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaProductosVenta);
        tablaProductosVenta.getAccessibleContext().setAccessibleName("tablaProductosVenta");
        tablaProductosVenta.getAccessibleContext().setAccessibleDescription("");

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 160, 570, 140));

        jButton1.setText("Añadir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 180, 80, -1));

        eliminar.setText("Borrar");
        eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarActionPerformed(evt);
            }
        });
        jPanel1.add(eliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 230, 80, -1));

        jLabel7.setText("SUB TOTAL:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 310, 80, 20));

        jLabel8.setText("DESCUENTOS:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 340, -1, 20));

        jLabel9.setText("IVA:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 370, -1, 20));

        jLabel10.setText("RETE FNTE:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 400, 80, 20));

        jLabel11.setText("RETEICA:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 400, 70, 20));

        jLabel12.setText("RETEIVA:");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 400, 60, 20));

        jLabel13.setText("TOTAL:");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 450, 50, 20));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Search.PNG"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 80, 20, 20));

        jLabel14.setText("DATOS DEL VENDEDOR");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, 140, -1));

        jLabel15.setText("DATOS DEL CLIENTE");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, 140, 20));

        jTextField1.setEditable(false);
        jTextField1.setEnabled(false);
        jTextField1.setOpaque(false);
        jPanel1.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 50, 110, -1));

        jTextField2.setEditable(false);
        jTextField2.setEnabled(false);
        jTextField2.setOpaque(false);
        jPanel1.add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, 130, -1));

        jTextField3.setEditable(false);
        jTextField3.setText("Vendedor");
        jTextField3.setEnabled(false);
        jTextField3.setOpaque(false);
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 50, 110, -1));

        nombre_cliente_venta_actual.setEditable(false);
        nombre_cliente_venta_actual.setEnabled(false);
        nombre_cliente_venta_actual.setOpaque(false);
        jPanel1.add(nombre_cliente_venta_actual, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 130, 130, -1));

        id_cliente_venta_actual.setEditable(false);
        id_cliente_venta_actual.setEnabled(false);
        id_cliente_venta_actual.setOpaque(false);
        jPanel1.add(id_cliente_venta_actual, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 130, 120, -1));

        jTextField6.setEditable(false);
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField6.setText(" $              0");
        jTextField6.setEnabled(false);
        jPanel1.add(jTextField6, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 310, 70, -1));

        jTextField7.setEditable(false);
        jTextField7.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField7.setText("0");
        jTextField7.setEnabled(false);
        jPanel1.add(jTextField7, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 340, 70, -1));

        jTextField8.setEditable(false);
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField8.setText("0");
        jTextField8.setEnabled(false);
        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });
        jPanel1.add(jTextField8, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 370, 70, -1));

        jTextField9.setEditable(false);
        jTextField9.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField9.setText("0");
        jTextField9.setEnabled(false);
        jPanel1.add(jTextField9, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 400, 40, -1));

        jTextField10.setEditable(false);
        jTextField10.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField10.setText("0");
        jTextField10.setEnabled(false);
        jPanel1.add(jTextField10, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 400, 40, -1));

        jTextField11.setEditable(false);
        jTextField11.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField11.setText("0");
        jTextField11.setEnabled(false);
        jPanel1.add(jTextField11, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 400, 40, -1));

        jTextField12.setEditable(false);
        jTextField12.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField12.setText("  $                0  ");
        jTextField12.setEnabled(false);
        jPanel1.add(jTextField12, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 450, 80, -1));

        pago.setText("REALIZAR PAGO");
        pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pagoActionPerformed(evt);
            }
        });
        jPanel1.add(pago, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 500, -1, -1));

        printBoton.setText("Print");
        printBoton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printBotonActionPerformed(evt);
            }
        });
        jPanel1.add(printBoton, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 40, -1, -1));

        jTabbedPane1.addTab("FACTURACION", jPanel1);

        jPanel2.setLayout(null);

        jTable2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                { new Integer(111),  new Integer(1112), "Juan rios", "12/20/2019",  new Double(300000.0)},
                { new Integer(112),  new Integer(1545), "Hernesto V", "13/08/2019",  new Double(250000.0)},
                { new Integer(113),  new Integer(5334), "Mariana Pagon", "14/05/2019",  new Double(360000.0)},
                { new Integer(114),  new Integer(5666), "Alejandro Maldonada", "15/06/2019",  new Double(352000.0)}
            },
            new String [] {
                "N FACTURA", "ID", "NOMBRES", "FECHA", "V/FACTURA"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jPanel2.add(jScrollPane2);
        jScrollPane2.setBounds(10, 110, 710, 90);

        jTextField13.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.add(jTextField13);
        jTextField13.setBounds(270, 50, 130, 30);

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/buscar.PNG"))); // NOI18N
        jButton4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(jButton4);
        jButton4.setBounds(400, 50, 70, 30);

        anularFactura.setText("Anular Factura");
        anularFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                anularFacturaActionPerformed(evt);
            }
        });
        jPanel2.add(anularFactura);
        anularFactura.setBounds(343, 240, 130, 30);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Numero Identificacion", "Numero Factura" }));
        jPanel2.add(jComboBox1);
        jComboBox1.setBounds(19, 50, 220, 30);

        jLabel16.setText("Buscar Por:");
        jPanel2.add(jLabel16);
        jLabel16.setBounds(30, 30, 70, 15);

        jTabbedPane1.addTab("VER FACTURAS", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 570, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pagoActionPerformed
        int cantidadProductos = tablaProductosVenta.getRowCount();
        if (cantidadProductos > 0 && total_venta > 0) {
            if (cliente_venta_actual != null) {
                int i = JOptionPane.showConfirmDialog(this, "¿Esta seguro de realizar pago?", "Advertencia", JOptionPane.YES_NO_OPTION);
                if (i == 0) {
                    Float subtotal_venta = (total_venta - total_iva - total_descuentos);
                    DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
                    Date date = Calendar.getInstance().getTime();
                    Factura f = new Factura(cliente_venta_actual.getIdentificacion(), empleado_actual.getIdentificacion(), 0, formatoFecha.format(date), "", 0.0f, 0.0f, 0.0f, subtotal_venta, total_iva, total_descuentos, total_venta, false);
                    int id_factura = facturaDB.insert(f);
                    facturas_array =  facturaDB.list();
                    this.actualizarTabla();
                    contadorVentas += 1;
                    formasdepago obj = new formasdepago("factura", total_venta, id_factura, cliente_venta_actual);
                    General.Escritorio.add(obj);
                    Dimension desktopSize = Escritorio.getSize();
                    Dimension FrameSize = obj.getSize();
                    obj.setLocation((desktopSize.width - FrameSize.width) / 2, (desktopSize.height - FrameSize.height) / 2);
                    obj.toFront();
                    obj.setVisible(true);
                }
            } else {
                JOptionPane.showMessageDialog(this, "No se ha seleccionado ningún cliente para esta factura.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "No se ha registrado ningún producto para esta factura.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_pagoActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        gestionarCliente cli = new gestionarCliente();
        General.Escritorio.add(cli);
        Dimension desktopSize = Escritorio.getSize();
        Dimension FrameSize = cli.getSize();
        cli.setLocation((desktopSize.width - FrameSize.width) / 2, (desktopSize.height - FrameSize.height) / 2);
        cli.toFront();
        cli.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        gestionarInventario inv = new gestionarInventario();
        General.Escritorio.add(inv);
        Dimension desktopSize = Escritorio.getSize();
        Dimension FrameSize = inv.getSize();
        inv.setLocation((desktopSize.width - FrameSize.width) / 2, (desktopSize.height - FrameSize.height) / 2);
        inv.toFront();
        inv.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarActionPerformed
        try {
            int fila = tablaProductosVenta.getSelectedRow();
            tablaProductosVenta.getValueAt(fila, 0);
            int i = JOptionPane.showConfirmDialog(this, "¿Esta seguro de eliminar a este producto de la factura?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if (i == 0) {
                DefaultTableModel modelo = (DefaultTableModel) tablaProductosVenta.getModel();
                modelo.removeRow(fila);
                this.recalcularValores();
                productos_venta.remove(fila);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "No ha seleccionado correctamente un producto para eliminar.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_eliminarActionPerformed

    private void anularFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_anularFacturaActionPerformed
        try {
            int fila = jTable2.getSelectedRow();
            String id = (String) jTable2.getValueAt(fila, 0);
            int i = JOptionPane.showConfirmDialog(this, "¿Esta seguro de anular esta factura?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if (i == 0) {
                facturaDB.anular(Integer.parseInt(id));
                facturas_array = facturaDB.list();
                this.actualizarTabla();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "No ha seleccionado ninguna factura para anular.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_anularFacturaActionPerformed

    private void tablaProductosVentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaProductosVentaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            System.out.println("Me oprimiste!!");
        }
    }//GEN-LAST:event_tablaProductosVentaKeyPressed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void printBotonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printBotonActionPerformed
        javax.swing.JFileChooser jF1 = new javax.swing.JFileChooser();
        String ruta = "";
        try {
            if (jF1.showSaveDialog(null) == jF1.APPROVE_OPTION) {
                ruta = jF1.getSelectedFile().getAbsolutePath();
                System.out.println("Ruta: " + ruta);
                exportarPDF(ruta);
                JOptionPane.showMessageDialog(null, "Se ha generado su reporte PDF satisfactoriamente", "PDF", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "¡ERROR!, " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_printBotonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton anularFactura;
    private javax.swing.JButton eliminar;
    public static javax.swing.JTextField id_cliente_venta_actual;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    public static javax.swing.JTextField nombre_cliente_venta_actual;
    private javax.swing.JButton pago;
    private javax.swing.JButton printBoton;
    public static javax.swing.JTable tablaProductosVenta;
    // End of variables declaration//GEN-END:variables
}
