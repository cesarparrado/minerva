/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal;

import java.util.*;

public class Empleado extends Persona {
    private String Eps;
    private String Arl;
    private String Pension;

    public Empleado(String Eps, String Pension, String Arl, String TipoIdentifiacion, String Identificacion, String Nombres, String Apellidos, String Celular, String Correo, String Direccion) {
        super(TipoIdentifiacion, Identificacion, Nombres, Apellidos, Celular, Correo, Direccion);
        this.Eps = Eps;
        this.Pension = Pension;
        this.Arl = Arl;
    }

    public String getEps() {
        return Eps;
    }

    public void setEps(String Eps) {
        this.Eps = Eps;
    }

    public String getArl() {
        return Arl;
    }

    public void setArl(String Arl) {
        this.Arl = Arl;
    }

    public String getPension() {
        return Pension;
    }

    public void setPension(String Pension) {
        this.Pension = Pension;
    }

}