/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Personal;

import java.util.*;

/**
 * 
 */
public class Cargo_empleado {

    /**
     * Default constructor
     */
    public Cargo_empleado() {
    }

    /**
     * 
     */
    private String Cargo;

    /**
     * 
     */
    private Date Fecha_Inicial;

    /**
     * 
     */
    private Date Fecha_Final;

    /**
     * 
     */
    private Float Salario;




    /**
     * @param Cargo 
     * @param Fecha_Inicial 
     * @param Valor
     */
    public void Cargo(String Cargo, Date Fecha_Inicial, Float Valor) {
        this.Cargo = Cargo;
        this.Fecha_Final = Fecha_Inicial;
        this.Salario = Valor;
    }

    /**
     * @param Fecha_Final
     */
    public void FinalizarCargo(Date Fecha_Final) {
        this.Fecha_Final = Fecha_Final;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String Cargo) {
        this.Cargo = Cargo;
    }

    public Date getFecha_Inicial() {
        return Fecha_Inicial;
    }

    public void setFecha_Inicial(Date Fecha_Inicial) {
        this.Fecha_Inicial = Fecha_Inicial;
    }

    public Date getFecha_Final() {
        return Fecha_Final;
    }

    public void setFecha_Final(Date Fecha_Final) {
        this.Fecha_Final = Fecha_Final;
    }

    public Float getSalario() {
        return Salario;
    }

    public void setSalario(Float Salario) {
        this.Salario = Salario;
    }
    
    

}