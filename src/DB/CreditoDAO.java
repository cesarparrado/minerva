/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import Cartera.Credito;
import Personal.Empleado;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author cesarparrado
 */
public class CreditoDAO {

    Connection connection;
    String tableName;

    public CreditoDAO(Connection connection) {
        this.connection = connection;
        this.tableName = "credito";
    }

    public ArrayList<Credito> list() {
        ArrayList<Credito> array = new ArrayList<Credito>();
        ResultSet rs = null;
        Statement s = null;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT * FROM " + this.tableName);
            while (rs.next()) {
                String factura_id = rs.getString("factura_id");
                if (factura_id == null) {
                    factura_id = "-1";
                }
                Credito c = new Credito(Integer.parseInt(rs.getString("id")), rs.getString("cliente_id"), Integer.parseInt(rs.getString("dias_de_pago")), rs.getString("fecha"), rs.getString("hora"), Float.parseFloat(rs.getString("monto_abonado")), Integer.parseInt(factura_id), rs.getString("estado"));
                array.add(c);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return array;
    }

    public int insert(Credito c) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO " + this.tableName + " (cliente_id, dias_de_pago, fecha, hora, monto_abonado, estado) VALUES ('" + c.getId_cliente() + "'," + c.getDias_de_pago()+ ",'" + c.getFecha() + "','" + c.getHora() + "'," + c.getMonto_Abonado() + ",'" + c.getEstado() + "')");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al agregar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }

    public int asignarFactura(int pk, int id_factura) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE "+this.tableName+" SET factura_id = " + id_factura + " WHERE id = " + pk + ";");
            if (z == 1) {
                System.out.println("Se asignado la factura el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al asignar la factura el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }

    public int anular(String pk) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE "+this.tableName+" SET estado = 'ANULADO' WHERE id = '" + pk + "'");
            if (z == 1) {
                System.out.println("Se elimino el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al eliminar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }
}
