/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;


import Inventario.Producto;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author cesarparrado
 */
public class ProductoDAO {

    Connection connection;
    String tableName;

    public ProductoDAO(Connection connection) {
        this.connection = connection;
        this.tableName = "producto";
    }

    public ArrayList<Producto> list() {
        ArrayList<Producto> array = new ArrayList<Producto>();
        ResultSet rs = null;
        Statement s = null;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT * FROM " + this.tableName);
            while (rs.next()) {
                Producto c = new Producto(Integer.parseInt(rs.getString("codigo")),rs.getString("nombre"),rs.getString("marca"),rs.getString("categoria"), Integer.parseInt(rs.getString("stock")), Float.parseFloat(rs.getString("iva")), Float.parseFloat(rs.getString("precio")),rs.getString("descripcion"),rs.getString("ref1"),rs.getString("ref2"),rs.getString("ref3"),rs.getString("ref4"));
                array.add(c);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return array;
    }

    /*public int insert(Cliente c) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO cliente (Identificacion, TipoIdentificacion, Nombres, Apellidos, Celular, Correo, Direccion, Monto_de_retencion) VALUES ('" + c.getIdentificacion() + "','" + c.getTipoIdentificacion() + "','" + c.getNombres() + "','" + c.getApellidos() + "','" + c.getCelular() + "','" + c.getCorreo() + "','" + c.getDireccion() + "','" + c.getMonto_de_retencion() + "')");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al agregar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }

    public int update(String pk, Cliente c) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE cliente SET Identificacion = '" + c.getIdentificacion() + "', TipoIdentificacion = '" + c.getTipoIdentificacion() + "', Nombres = '" + c.getNombres() + "', Apellidos = '" + c.getApellidos() + "', Celular = '" + c.getCelular() + "', Correo = '" + c.getCorreo() + "', Direccion = '" + c.getDireccion() + "', Monto_de_retencion = '" + c.getMonto_de_retencion() + "' WHERE Identificacion = '" + pk + "';");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al agregar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }

    public int delete(String pk) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("DELETE FROM cliente WHERE Identificacion = '" + pk + "'");
            if (z == 1) {
                System.out.println("Se elimino el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al eliminar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }*/

}
