package DB;


import java.sql.*;

public class Conexion {

    private static Connection conn = null;
    private String driver;
    private String url;
    private String usuario;
    private String password;

    private Conexion() {

        String url = "jdbc:mysql://localhost:3306/minerva";
        String driver = "com.mysql.jdbc.Driver";
        String usuario = "minerva";
        String password = "aFKLPsmT3jiykl6L";

        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, usuario, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {

        if (conn == null) {
            new Conexion();
        }

        return conn;
    }
}
