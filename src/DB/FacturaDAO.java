/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import Cartera.Credito;
import Personal.Empleado;
import Ventas.Factura;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author cesarparrado
 */
public class FacturaDAO {

    Connection connection;
    String tableName;

    public FacturaDAO(Connection connection) {
        this.connection = connection;
        this.tableName = "factura";
    }

    public ArrayList<Factura> list() {
        ArrayList<Factura> array = new ArrayList<Factura>();
        ResultSet rs = null;
        Statement s = null;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT * FROM " + this.tableName);
            while (rs.next()) {
                Factura f = new Factura(rs.getString("cliente_id"), rs.getString("empleado_id"), Integer.parseInt(rs.getString("id")), rs.getString("fecha"), rs.getString("forma_de_pago"), Float.parseFloat(rs.getString("rete_fuente")), Float.parseFloat(rs.getString("rete_iva")), Float.parseFloat(rs.getString("rete_ica")), Float.parseFloat(rs.getString("subtotal")), Float.parseFloat(rs.getString("iva")), Float.parseFloat(rs.getString("descuento")), Float.parseFloat(rs.getString("total")), ((Integer.parseInt(rs.getString("estado")) == 1) ? true : false));
                array.add(f);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return array;
    }

    public int insert(Factura f) {
        Statement s = null;
        ResultSet rs = null;
        int result = -1;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO " + this.tableName + " (empleado_id, cliente_id, fecha, forma_de_pago, rete_fuente, rete_iva, rete_ica, subtotal, iva, descuento, total, estado) VALUES ('" + f.getEmpleado() + "','" + f.getCliente() + "','" + f.getFecha() + "','" + f.getForma_de_pago() + "'," + f.getRete_Fuente() + "," + f.getRete_iva() + "," + f.getRete_ica() + "," + f.getSubtotal() + "," + f.getIVA() + "," + f.getDescuento() + "," + f.getTotal() + "," + ((f.getAnulada()) ? 1 : 0) + ")");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
                rs = s.executeQuery("SELECT LAST_INSERT_ID() as last_id");
                while (rs.next()) {
                    result = Integer.parseInt(rs.getString("last_id"));
                }
                return result;
            } else {
                System.out.println("Ocurrio un problema al agregar el registro");
                return result;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return result;
        }
    }

    public int asignarFactura(int pk, int id_factura) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE " + this.tableName + " SET factura_id = " + id_factura + " WHERE id = " + pk + ";");
            if (z == 1) {
                System.out.println("Se asignado la factura el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al asignar la factura el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }

    public int anular(int pk) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE " + this.tableName + " SET estado = 1 WHERE id = " + pk );
            if (z == 1) {
                System.out.println("Se elimino el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al eliminar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }
}
