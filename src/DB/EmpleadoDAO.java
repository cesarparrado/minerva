/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import Personal.Empleado;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author cesarparrado
 */
public class EmpleadoDAO {

    Connection connection;
    String tableName;

    public EmpleadoDAO(Connection connection) {
        this.connection = connection;
        this.tableName = "empleado";
    }

    public ArrayList<Empleado> list() {
        ArrayList<Empleado> array = new ArrayList<Empleado>();
        ResultSet rs = null;
        Statement s = null;
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT * FROM " + this.tableName);
            while (rs.next()) {
                Empleado c = new Empleado(rs.getString("Eps"), rs.getString("Pension"), rs.getString("Arl"), rs.getString("TipoIdentificacion"), rs.getString("Identificacion"), rs.getString("Nombres"), rs.getString("Apellidos"), rs.getString("Celular"), rs.getString("Correo"), rs.getString("Direccion"));
                array.add(c);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
        return array;
    }

    public int insert(Empleado c) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO empleado (Identificacion, TipoIdentificacion, Nombres, Apellidos, Celular, Correo, Direccion, Eps, Arl, Pension) VALUES ('" + c.getIdentificacion() + "','" + c.getTipoIdentificacion() + "','" + c.getNombres() + "','" + c.getApellidos() + "','" + c.getCelular() + "','" + c.getCorreo() + "','" + c.getDireccion() + "','" + c.getEps() + "','" + c.getArl() + "','" + c.getPension() + "')");
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al agregar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }
    
    public int update(String pk, Empleado c) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE empleado SET Identificacion = '" + c.getIdentificacion() + "', TipoIdentificacion = '" + c.getTipoIdentificacion() + "', Nombres = '" + c.getNombres() + "', Apellidos = '" + c.getApellidos() + "', Celular = '" + c.getCelular() + "', Correo = '" + c.getCorreo() + "', Direccion = '" + c.getDireccion() + "', Eps = '" + c.getEps() + "', Arl = '" + c.getEps() + "', Pension  = '" + c.getEps() + "'  WHERE Identificacion = " + pk );
            if (z == 1) {
                System.out.println("Se agrego el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al agregar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }

    public int delete(String pk) {
        Statement s = null;
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("DELETE FROM empleado WHERE Identificacion = " + pk );
            if (z == 1) {
                System.out.println("Se elimino el registro de manera exitosa");
                return 1;
            } else {
                System.out.println("Ocurrio un problema al eliminar el registro");
                return -1;
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return -1;
        }
    }

}
